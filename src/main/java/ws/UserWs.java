package ws;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ajc.training.dao.DaoUtilisateur;
import com.ajc.training.entities.Utilisateur;

//has neither @WebService nor @WebServiceProvider annotation
@Path("/user")
public class UserWs {

	private DaoUtilisateur dao;
	
	public UserWs() {
		dao = new DaoUtilisateur();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/single/{idUser}")
	public Utilisateur userInfo(@PathParam("idUser") int idUser) {
		return dao.getUserInfo(idUser);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/userlist")
	public List<Utilisateur> userList(){
		return dao.getUserList();
	}
	
	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	public  Boolean updateUser(@BeanParam Utilisateur utilisateur) {
		System.out.println("ws update");
		System.out.println(utilisateur.toString());
		return dao.updateUser(utilisateur);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/login")
	public Utilisateur login(@QueryParam("login") String login , @QueryParam("pw") String pw) {
		return dao.loginCorrect(login,pw);
	}
	
}
