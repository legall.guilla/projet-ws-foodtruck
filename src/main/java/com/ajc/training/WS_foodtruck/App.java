package com.ajc.training.WS_foodtruck;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;
import javax.xml.ws.Endpoint;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.hibernate.Session;

import com.ajc.training.dao.HibernateUtils;

import ws.UserWs;


public class App 
{

	public static final URI BASE_URI = getBaseURI();
	
    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/rest/").port(9991).build();
    }
    
    public static void main( String[] args )
    {
    	
    	ResourceConfig rc = new ResourceConfig();
        rc.packages("ws");
        
    	Session s = HibernateUtils.getSession();
    	s.close();
    	
    	 HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, rc);
         try {
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

    }
}
