package com.ajc.training.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import com.ajc.training.entities.Utilisateur;

public class DaoUtilisateur {
	
	private Session s;
	
	public Utilisateur getUserInfo(int idUser) {
		s = HibernateUtils.getSession();
		Utilisateur u = s.get(Utilisateur.class, idUser);
		s.close();
		return u;
	}

	public List<Utilisateur> getUserList() {
		System.out.println("requete user list");
		List<Utilisateur> res = null;
		try {
			s = HibernateUtils.getSession();
			CriteriaQuery<Utilisateur> criteriaQuery = s.getCriteriaBuilder().createQuery(Utilisateur.class);
			criteriaQuery.from(Utilisateur.class);
	        res = s.createQuery(criteriaQuery).getResultList();
	        s.close();
		}catch(Exception e) {
			System.out.println(e.toString());
		}

        return res;
	}

	public Boolean updateUser(Utilisateur utilisateur) {
		System.out.println("updateUser");
		System.out.println(utilisateur.toString());
		try {
			System.out.println("1");
			s = HibernateUtils.getSession();
			System.out.println("2");
			s.merge(utilisateur);
			System.out.println("3");
		}catch(Exception e) {
			System.out.println("erreur");
			return Boolean.FALSE;
		}
		System.out.println("success");
		return Boolean.TRUE;
	}

	public Utilisateur loginCorrect(String login, String pw) {
		s = HibernateUtils.getSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<Utilisateur> cr = cb.createQuery(Utilisateur.class);
		Root<Utilisateur> root  = cr.from(Utilisateur.class);
		cr.select(root ).where(cb.like(root.<String>get("login"), login),cb.like(root.<String>get("password"), pw));
		Utilisateur ut = s.createQuery( cr ).getSingleResult();
		return ut;
	}

}
