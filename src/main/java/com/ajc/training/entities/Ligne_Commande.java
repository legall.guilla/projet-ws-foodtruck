package com.ajc.training.entities;

import javax.persistence.*;


@Entity
@NamedQueries({
	@NamedQuery(name="Ligne_Commande.findAll", query = "from Ligne_Commande"),
	@NamedQuery(name="Ligne_Commande.findById", query = "from Ligne_Commande where id = :myID")
})
public class Ligne_Commande {
	/*********************************************************************
	 * Properties
	 *********************************************************************/
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(nullable = false, name = "id_ligne_commande")
	private Integer id;
	private Integer quantite;
	private double prix_unitaire;
	@ManyToOne
	@JoinColumn(name="id_commande")
	private Commande commande;
	@OneToOne
	@JoinColumn(
		name= "id_produit",
		referencedColumnName="id_produit"
	)
	private Produit produit;
	
	/*********************************************************************
	 * Gettes / Setters
	 *********************************************************************/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantite() {
		return quantite;
	}

	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	public double getPrix_unitaire() {
		return prix_unitaire;
	}

	public void setPrix_unitaire(double prix_unitaire) {
		this.prix_unitaire = prix_unitaire;
	}

	public Commande getCommande() {
		return commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	public Produit getProduit() {
		return produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}


	/*********************************************************************
	 * Constructors
	 *********************************************************************/
	
	public Ligne_Commande() {
		
	}

	public Ligne_Commande( Integer quantite,double prix_unitaire) {
		this.quantite 		= quantite;
		this.prix_unitaire 	= prix_unitaire	;
	}
	
}
