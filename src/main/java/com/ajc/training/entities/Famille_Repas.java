package com.ajc.training.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * @author godwinavodagbe
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "Famille_Repas.findAll", query = "from Famille_Repas"),
	@NamedQuery(name = "Famille_Repas.findById", query = "from Famille_Repas where id = :myID")
})
public class Famille_Repas {
	
	/*********************************************************************
	 * Properties
	 *********************************************************************/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_famille_repas")
	private Integer id;
	private String libelle_famille_repas; 
	private boolean isenabled;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			joinColumns=@JoinColumn(name="id_famille_repas"),
			inverseJoinColumns=@JoinColumn(name="id_repas")
	)
	private List<Repas> repas;
	
	@OneToMany(mappedBy="famille_repas",cascade = {CascadeType.REMOVE})
	private List<Produit> produitList;
	
	

	/*********************************************************************
	 * Gettes / Setters
	 *********************************************************************/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle_famille_repas() {
		return libelle_famille_repas;
	}

	public void setLibelle_famille_repas(String libelle_famille_repas) {
		this.libelle_famille_repas = libelle_famille_repas;
	}

	public boolean isIsenabled() {
		return isenabled;
	}

	public void setIsenabled(boolean isenabled) {
		this.isenabled = isenabled;
	}

	public List<Repas> getRepas() {
		return repas;
	}

	public void setRepas(List<Repas> repas) {
		this.repas = repas;
	}
	

	public List<Produit> getProduitList() {
		return produitList;
	}

	public void setProduitList(List<Produit> produitList) {
		this.produitList = produitList;
	}
		
	
	/*********************************************************************
	 * Constructors
	 *********************************************************************/
	public Famille_Repas() {
		
	}
	
	public Famille_Repas (String libelle_famille_repas, boolean isenabled) {
		this.libelle_famille_repas 	= libelle_famille_repas;
		this.isenabled 	= isenabled;
	}
	
}
