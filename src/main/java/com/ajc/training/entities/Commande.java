package com.ajc.training.entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Table;
import org.hibernate.annotations.Tables;


@Entity
@NamedQueries({
	@NamedQuery(name="Commande.findAll", query = "from Commande"),
	@NamedQuery(name="Commande.findById", query = "from Commande where id = :myID")
})
public class Commande {

	/*********************************************************************
	 * Properties
	 *********************************************************************/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "id_commande")
	private Integer id;
	private double prix_total;
	@Temporal(TemporalType.DATE)
	private Calendar date_commande;
	private String num_facture;
	
	// Associations
	@ManyToOne
	@JoinColumn(name="id_utilisateur")
	private Utilisateur userCommande;
	
	@OneToOne
	@JoinColumn(
				name= "id_statut_commande",
				referencedColumnName="id_statut_commande"
			)
	private Statut_Commande statut_commande;
	
	@OneToOne
	@JoinColumn(
			name= "id_adresse_livraison",
			referencedColumnName="id_adresse_livraison"
			)
	private Adresse_Livraison adresse_livraison;
	
	@OneToOne
	@JoinColumn(
				name= "id_adresse_facturation",
				referencedColumnName="id_adresse_facturation"
			)
	private Adresse_Facturation adresse_facturation;
	

	@OneToMany(mappedBy="commande",cascade = {CascadeType.ALL})
	private List<Ligne_Commande> ligneCommandeList;
	
	/*********************************************************************
	 * Gettes / Setters
	 *********************************************************************/

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getPrix_total() {
		return prix_total;
	}

	public void setPrix_total(double prix_total) {
		this.prix_total = prix_total;
	}

	public Calendar getDate_commande() {
		return date_commande;
	}

	public void setDate_commande(Calendar date_commande) {
		this.date_commande = date_commande;
	}

	public String getNum_facture() {
		return num_facture;
	}

	public void setNum_facture(String num_facture) {
		this.num_facture = num_facture;
	}

	public Utilisateur getUserCommande() {
		return userCommande;
	}

	public void setUserCommande(Utilisateur userCommande) {
		this.userCommande = userCommande;
	}

	public Statut_Commande getStatut_commande() {
		return statut_commande;
	}

	public void setStatut_commande(Statut_Commande statut_commande) {
		this.statut_commande = statut_commande;
	}

	public Adresse_Livraison getAdresse_livraison() {
		return adresse_livraison;
	}

	public void setAdresse_livraison(Adresse_Livraison adresse_livraison) {
		this.adresse_livraison = adresse_livraison;
	}

	public Adresse_Facturation getAdresse_facturation() {
		return adresse_facturation;
	}

	public void setAdresse_facturation(Adresse_Facturation adresse_facturation) {
		this.adresse_facturation = adresse_facturation;
	}
	
	
	public List<Ligne_Commande> getLigneCommandeList() {
		return ligneCommandeList;
	}

	public void setLigneCommandeList(List<Ligne_Commande> ligneCommandeList) {
		this.ligneCommandeList = ligneCommandeList;
	}

	/*********************************************************************
	 * Constructors
	 *********************************************************************/
	public Commande() {
		
	}

	public Commande(double prix_total, Calendar date_commande, String num_facture) {
		this.prix_total 	= prix_total; 
		this.date_commande 	= date_commande;
		this.num_facture 	= num_facture;
	}
}
