package com.ajc.training.entities;

import javax.persistence.*;


@Entity
@NamedQueries({
	@NamedQuery(name="Actualites.findAll", query = "from Actualites"),
	@NamedQuery(name="Actualites.findById", query = "from Actualites where id = :myID")
})
public class Actualites {
	/*********************************************************************
	 * Properties
	 *********************************************************************/
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(nullable = false, name = "id_actualites")
	private Integer id;
	private String titre;
	private String description;
	private String image;
	private Boolean isenable;
	
	/*********************************************************************
	 * Gettes / Setters
	 *********************************************************************/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getIsenable() {
		return isenable;
	}

	public void setIsenable(Boolean isenable) {
		this.isenable = isenable;
	}

	/*********************************************************************
	 * Constructors
	 *********************************************************************/
	public Actualites() {
		
	}

	public Actualites( String titre, String description, String image, Boolean isenable) {
		this.titre 		= titre;
		this.description= description;
		this.image 		= image;
		this.isenable	= isenable;
	}
	
}
