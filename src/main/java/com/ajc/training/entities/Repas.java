package com.ajc.training.entities;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.SimpleTimeZone;

import javax.persistence.*;

@Entity
@NamedQueries({
	@NamedQuery(name = "Repas.findAll", query = "from Repas"),
	@NamedQuery(name = "Repas.findById", query = "from Repas where id = :myID")
})
public class Repas {
	
	/*********************************************************************
	 * Properties
	 *********************************************************************/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, name = "id_repas")
	private Integer id;
	//@Basic(fetch = FetchType.LAZY, optional = true)
	private String libelle_repas;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar heure_limite;
	private boolean isenabled;
	@ManyToMany(mappedBy = "repas", cascade = CascadeType.PERSIST)
	private List<Famille_Repas> famille_repas;
	
	
	/*********************************************************************
	 * Gettes / Setters
	 *********************************************************************/
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle_repas() {
		return libelle_repas;
	}

	public void setLibelle_repas(String libelle_repas) {
		this.libelle_repas = libelle_repas;
	}

	public Calendar getHeure_limite() {
		return heure_limite;
	}

	public void setHeure_limite(Calendar heure_limite) {
		this.heure_limite = heure_limite;
	}

	public boolean isIsenabled() {
		return isenabled;
	}

	public void setIsenabled(boolean isenabled) {
		this.isenabled = isenabled;
	}

	public List<Famille_Repas> getFamille_repas() {
		return famille_repas;
	}

	public void setFamille_repas(List<Famille_Repas> famille_repas) {
		this.famille_repas = famille_repas;
	}
	
	/*********************************************************************
	 * Constructors
	 *********************************************************************/
	
	public Repas() {
		
	}
	
	public Repas(String libelle_repas, Calendar heure_limite, boolean isenabled) {
		this.libelle_repas 	= libelle_repas;
		this.heure_limite 	= heure_limite;
		this.isenabled		= isenabled;
	}
}
