-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: food_truck
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `actualites`
--

LOCK TABLES `actualites` WRITE;
/*!40000 ALTER TABLE `actualites` DISABLE KEYS */;
INSERT INTO `actualites` VALUES (1,'Venez feter avec nous la grande ouverture de notre food-truck ce samedi 01 decembre 2018.','https://www.nrn.com/sites/nrn.com/files/styles/article_featured_retina/public/zaxbys-food-truck-1.gif?itok=xNt8SR_e',1,'Inauguration de l ouverture notre Food Truck'),(2,'Nous realisons une ristourne de 25% pour les 100 premières commandes','http://res.cloudinary.com/hksqkdlah/image/upload/w_1200,h_630,c_fill/ATK%20Food%20Truck/CAN_ATKFoodTruck_5391_23x9',1,'Promotion exceptionnelle pour les 100 premières commandes'),(3,'Cette semaine, nous vous proposons un veloute de potimarrons, servis avec ses croutons à l ail','http://www.seblartisanculinaire.com/wp-content/uploads/2017/11/seb-camion-food-truck.png',1,'La soupe du jour');
/*!40000 ALTER TABLE `actualites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `adresse_facturation`
--

LOCK TABLES `adresse_facturation` WRITE;
/*!40000 ALTER TABLE `adresse_facturation` DISABLE KEYS */;
INSERT INTO `adresse_facturation` VALUES (1,'78415',1,8,'France','rue Edouard','Flins',1),(2,'91300',1,7,'France','rue Jean Baptiste Charcot','Massy',1),(3,'59000',1,56,'France','Place du marche','Lille',1),(4,'75009',1,19,'France','Rue Rougemont','Paris',1),(5,'78410',1,14,'France','Rue des Sources','Aubergenville',2);
/*!40000 ALTER TABLE `adresse_facturation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `adresse_livraison`
--

LOCK TABLES `adresse_livraison` WRITE;
/*!40000 ALTER TABLE `adresse_livraison` DISABLE KEYS */;
INSERT INTO `adresse_livraison` VALUES (1,'78415',1,8,'France','rue Edouard','Flins',1),(2,'91300',1,7,'France','rue Jean Baptiste Charcot','Massy',1),(3,'59000',1,56,'France','Place du marche','Lille',1),(4,'75009',1,19,'France','Rue Rougemont','Paris',1),(5,'78410',1,14,'France','Rue des Sources','Aubergenville',2);
/*!40000 ALTER TABLE `adresse_livraison` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `commande`
--

LOCK TABLES `commande` WRITE;
/*!40000 ALTER TABLE `commande` DISABLE KEYS */;
INSERT INTO `commande` VALUES (1,'2018-11-01','79KGR237',10.99,1,1,4,1),(2,'2018-11-12','05HBD986',10.99,2,2,4,1),(3,'2018-12-10','93DTB345',10.99,3,3,3,1);
/*!40000 ALTER TABLE `commande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `famille_repas`
--

LOCK TABLES `famille_repas` WRITE;
/*!40000 ALTER TABLE `famille_repas` DISABLE KEYS */;
INSERT INTO `famille_repas` VALUES (1,1,'Boissons froides'),(2,1,'Boissons chaudes'),(3,1,'Viennoiserie'),(4,1,'Pain'),(5,1,'Entree'),(6,1,'Plat principal'),(7,1,'Dessert'),(8,1,'Sans gluten'),(9,1,'Hambergeurs');
/*!40000 ALTER TABLE `famille_repas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `famille_repas_repas`
--

LOCK TABLES `famille_repas_repas` WRITE;
/*!40000 ALTER TABLE `famille_repas_repas` DISABLE KEYS */;
INSERT INTO `famille_repas_repas` VALUES (1,1),(1,2),(1,3),(1,4),(2,1),(2,2),(2,3),(2,4),(3,1),(3,3),(4,1),(4,2),(4,3),(4,4),(5,2),(5,4),(6,2),(6,4),(7,2),(7,3),(7,4);
/*!40000 ALTER TABLE `famille_repas_repas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'Mr'),(2,'Mlle'),(3,'Mme'),(4,'Autre');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ligne_commande`
--

LOCK TABLES `ligne_commande` WRITE;
/*!40000 ALTER TABLE `ligne_commande` DISABLE KEYS */;
INSERT INTO `ligne_commande` VALUES (1,4,2,1,1),(2,2,1,1,15),(3,5,1,1,13),(4,8,1,2,22),(5,15.5,1,2,23),(6,4,1,2,19),(7,1.5,3,3,6),(8,1,1,3,18),(9,4,2,3,17);
/*!40000 ALTER TABLE `ligne_commande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `produit`
--

LOCK TABLES `produit` WRITE;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` VALUES (1,'Eau plate 1/2l','Lundi, Mardi, Vendredi','http://www.hellopro.fr/images/produit-2/3/3/8/24-bouteilles-d-eaux-nestle-aquarel-50-cl-1915833.jpg',1,'Eau plate 1/2l','Eau plate 1/2l',4,250,1),(2,'Eau plate 1l','Lundi, Mardi, Vendredi','https://www.welcomeoffice.com/WO_Products_Images/Large/569808.jpg',1,'Eau plate 1l','Eau plate 1l',2.1,300,1),(3,'Eau gazeuse 1/2l','Lundi, Mardi, Vendredi','https://i2.cdscdn.com/pdt2/9/6/5/1/300x300/hig5054186625965/rw/highland-spring-encore-2l-eau-de-source-pack-de-6.jpg',1,'Eau gazeuse 1/2l','Eau gazeuse 1/2l',5.12,250,1),(4,'ice tea frais','Lundi, Vendredi','https://images-na.ssl-images-amazon.com/images/I/51fCDWlEF8L.jpg',1,'Ice Tea Peche 33cl','Ice Tea Peche 33cl',1.5,150,1),(5,'coca cola fraiche','Lundi, Mardi, Vendredi','https://aldente-pizza.fr/wp-content/uploads/2018/09/coca-cola-33cl.jpg',1,'Coca Cola 33cl','Coca Cola 33cl',1.5,150,1),(6,'Infusion au citron','Lundi, Mardi, Vendredi','https://vitaality.fr/wp-content/uploads/2016/01/©-sommai-Fotolia.com_.jpg',1,'the vert citron','the vert citron',1.99,150,2),(7,'cafe liegeois','Lundi, Mardi, Vendredi','http://www.la-cuisine-de-mes-racines.com/wp-content/uploads/2014/03/Cafe-liegeois.png',1,'cafe liegeois','cafe liegeois',2.45,100,2),(8,'the noir','Lundi, Mardi, Vendredi','https://ileauxepices.com/blog/wp-content/uploads/2013/05/the-noir.jpg',1,'the noir','the noir',1.99,230,2),(9,'Croissant au beurre','Lundi, Mardi, Vendredi','https://www.ptitchef.com/imgupl/recipe/croissant-au-beurre-en-moins-de-10-minutes-pate--46561p56914.jpg',1,'Croissant au beurre','Croissant au beurre',1.3,100,3),(10,'Croissant aux amandes','Lundi, Mardi, Vendredi','https://i.ytimg.com/vi/GEW_joMhM3Y/maxresdefault.jpg',1,'Croissant aux amandes','Croissant aux amandes',1.2,100,3),(11,'Torsade au raisin','Lundi, Mardi, Vendredi','http://www.lalosparis.com/wp-content/uploads/2016/10/Torsade.jpg',1,'Torsade au raisin','Torsade au raisin',1.5,100,3),(12,'Pain multicereales','Lundi, Mardi, Vendredi','http://www.marieblachere.com/wp-content/uploads/2013/10/cmapagne-multicereales-350x250.png',1,'Pain multicereales','Pain multicereales',1.1,100,4),(13,'Pain complet','Lundi, Mardi, Vendredi','https://previews.123rf.com/images/mayerkleinostheim/mayerkleinostheim1203/mayerkleinostheim120300778/12795491-le-pain-complet-avec-epi-de-ble.jpg',1,'Pain complet','Pain complet',1.2,100,4),(14,'pain traditionnel','Lundi, Mardi, Vendredi','https://1.bp.blogspot.com/-xbh7o4j02XQ/U6ZI8qRKj7I/AAAAAAAAQcA/Wb4NI5qa3zQ/s1600/IMG_2599.jpg',1,'Pain traditionnel','Pain traditionnel',1.35,100,4),(15,'Planche mixte','Lundi, Mardi, Vendredi','https://media-cdn.tripadvisor.com/media/photo-s/0d/71/fd/fd/la-petite-planche-mixte.jpg',1,'Planche mixte','Planche mixte',8.19,100,5),(16,'Soupe du jour','Lundi, Mardi, Vendredi','https://www.5amtag.ch/wp-content/uploads/2016/08/exotische_karottesuppe.jpg',1,'Soupe du jour','Soupe du jour',3.3,100,5),(17,'Salade cesar','Lundi, Mardi, Vendredi','https://img-3.journaldesfemmes.fr/jkqlaHD119wHCUpvp1OIw_Yk7NM=/750x/smart/image-icu/324872_1254930746.jpg',1,'Salade cesar','Salade cesar',5.4,100,5),(18,'Specialite savoyarde composee de pommes de terre, lardons, reblochon et oignon, et accompagnee d’une bonne salade verte','Lundi, Mardi, Vendredi','https://cdn-elle.ladmedia.fr/var/plain_site/storage/images/elle-a-table/recettes-de-cuisine/tartiflette-2072098/21837752-2-fre-FR/Tartiflette.jpg',1,'Tartiflette','Tartiflette',4.44,100,6),(19,'\',\'Plat traditionnel de la cuisine française, le coq au vin est accompagne d’oignons, de carottes, d’un bouquet garni et de champignons','Lundi, Mardi, Vendredi','https://static.cuisineaz.com/400x320/i130913-coq-au-vin-au-cookeo.jpeg',1,'Coq au vin','Coq au vin',6.79,100,6),(20,'Filet de porc dans sa sauce à la moutarde, accompagne de pomme de terre et haricots verts','Lundi, Mardi, Vendredi','https://www.picard.fr/on/demandware.static/-/Sites-catalog-picard/default/dw89abe746/produits/viandes-volailles/edition/000000000000015756_E1.jpg',1,'Filet de porc à la moutarde','Filet de porc à la moutarde',5.3,100,6),(21,'tarte faite maison','Lundi, Mardi, Vendredi','https://files.meilleurduchef.com/mdc/photo/recette/tarte-tatin/tarte-tatin-640.jpg',1,'Tarte tatin','tarte tatin',3.5,100,7),(22,'melange de chocolat noir et chocolat blanc','Lundi, Mardi, Vendredi','https://files.meilleurduchef.com/mdc/photo/recette/mousse-chocolat/mousse-chocolat-640.jpg',1,'Mousse au chocolat','Mousse au chocolat',1.3,500,7),(23,'Fruits frais produits en France','Lundi, Mardi, Vendredi','http://idata.over-blog.com/5/03/61/53/revue-de-presse-8/1-fruits.jpg',1,'Fruits de saison','Fruits de saison',1.9,100,7);
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `profil`
--

LOCK TABLES `profil` WRITE;
/*!40000 ALTER TABLE `profil` DISABLE KEYS */;
INSERT INTO `profil` VALUES (1,'Administrateur'),(2,'Commercial'),(3,'Gerant'),(4,'Client');
/*!40000 ALTER TABLE `profil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `repas`
--

LOCK TABLES `repas` WRITE;
/*!40000 ALTER TABLE `repas` DISABLE KEYS */;
INSERT INTO `repas` VALUES (1,'2018-12-17 15:17:42',1,'Petit dejeuner'),(2,'2018-12-17 15:17:42',1,'Dejeuner'),(3,'2018-12-17 15:17:42',1,'Gouter'),(4,'2018-12-17 15:17:42',1,'Diner');
/*!40000 ALTER TABLE `repas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `societe`
--

LOCK TABLES `societe` WRITE;
/*!40000 ALTER TABLE `societe` DISABLE KEYS */;
INSERT INTO `societe` VALUES (1,'92150','CAPGEMINI',5,'479766842','47976684200286','France','rue Frederic Clavel','Suresnes'),(2,'45000','OnePoint',22,'23345678987654567654','122333009','France','Champs de Mars','Marseille');
/*!40000 ALTER TABLE `societe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `statut_commande`
--

LOCK TABLES `statut_commande` WRITE;
/*!40000 ALTER TABLE `statut_commande` DISABLE KEYS */;
INSERT INTO `statut_commande` VALUES (1,'En cours de validation'),(2,'En cours de preparation'),(3,'En cours de livraison'),(4,'Commande livree'),(5,'Commande annulee');
/*!40000 ALTER TABLE `statut_commande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES (1,'2018-12-17','hello@ekoura.com',1,'gavodagbe','AVODAGBE','POO13332:deeefe8!','Godwin',1,4,1),(2,'2018-12-17','zribi-raouia@hotmail.fr',1,'RBenSalah','Ben Salah','RBenSalah','Raouia',2,1,2),(3,'2018-12-17','pierredupond@gmail.com',1,'PDupond','Dupond','PDupond!','Pierre',1,4,2),(4,'2018-12-17','fifibrindacier@yahoo.com',1,'FBrindacier','Brindacier','FBrindacier!','Fifi',1,3,1),(5,'2018-12-17','artie-chaud@gmail.com',1,'AChault','Chault','AChaulteeefe8!','Artie',1,4,1);
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-17 15:37:09
